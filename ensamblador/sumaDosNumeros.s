.section .data
primerNumero: .long 0
segundoNumero: .long 0
leerUno: .ascii "Introducir el primer número a sumar\0",
leerDos: .ascii "Introducir el segundo número a sumar\0",
resultado: .ascii "La suma es: %d\n"
tipo: .ascii "%d"

.section .bss

.section .text

.globl main

main:
	pushl	$leerUno
	call	printf
	addl	$4,	%esp
	pushl	$primerNumero
	pushl	$tipo
	call	scanf
	addl	$4,	%esp

	pushl	$leerDos
	call	printf
	addl	$4,	%esp
	pushl	$segundoNumero
	pushl	$tipo
	call	scanf
	addl	$8,	%esp

	movl	primerNumero,	%eax
	addl	segundoNumero,	%eax
	pushl		%eax
	pushl	$resultado
	call	printf
	addl	$4,	%esp
	movl	$0,	%ebx
	movl	$1,	%eax
	int	$0x80
